<?php
namespace Scalapay\Casestudy\Block;

use Magento\Framework\View\Element\Template;

/**
 * class OrderForm
 *
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class OrderForm extends Template
{
    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('scalapay/index/save', ['_secure' => true]);
    }
}
