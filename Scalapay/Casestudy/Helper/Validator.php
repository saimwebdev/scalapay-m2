<?php
namespace Scalapay\Casestudy\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * class Validator
 *
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class Validator extends AbstractHelper
{
    const PARAMETERS_TO_VALIDATE = [
        'consumer-phoneNumber',
        'consumer-givenNames',
        'consumer-surname',
        'consumer-email',
        'shipping-name',
        'shipping-countryCode',
        'shipping-postcode',
        'shipping-line1',
        'items-name',
        'items-category',
        'items-sku',
        'items-quantity',
        'items-price-amount',
        'items-price-currency',
        'totalAmount-amount',
        'totalAmount-currency',
        'merchant-redirectCancelUrl',
        'merchant-redirectConfirmUrl'
    ];

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function validate($data)
    {
        foreach ($this::PARAMETERS_TO_VALIDATE as $parameter) {
            if (!array_key_exists($parameter, $data)) {
                throw new \Exception("Parameter $parameter missing");
            }
        }

        return true;
    }
}
