<?php
namespace Scalapay\Casestudy\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\HTTP\Client\Curl;
use Scalapay\Casestudy\Helper\Configuration;

/**
 * class Webservice
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class Webservice extends AbstractHelper
{
    const BEARER_TOKEN = 'qhtfs87hjnc12kkos';

    /**
     * @var Curl
     */
    private $_curl;

    /**
     * @var Configuration
     */
    private $_configuration;

    /**
     * @param Context $context
     * @param Curl $curl
     */
    public function __construct(
        Context $context,
        Curl $curl,
        Configuration $configuration
    ) {
        parent::__construct($context);
        $this->_curl = $curl;
        $this->_configuration = $configuration;
    }

    /**
     * @param $parameters
     */
    public function build($parameters)
    {
        $payload =  [
            'consumer' => [
                'phoneNumber' => $parameters['consumer-phoneNumber'],
                'givenNames' => $parameters['consumer-givenNames'],
                'surname' => $parameters['consumer-surname'],
                'email' => $parameters['consumer-email']
            ],
            'shipping' => [
                'name' => $parameters['shipping-name'],
                'countryCode' => $parameters['shipping-countryCode'],
                'postcode' => $parameters['shipping-postcode'],
                'line1' => $parameters['shipping-line1']
            ],
            'items' => [
                [
                    'name' => $parameters['items-name'],
                    'category' => $parameters['items-category'],
                    'sku' => $parameters['items-sku'],
                    'quantity' => $parameters['items-quantity'],
                    'price' => [
                        'amount' => $parameters['items-price-amount'],
                        'currency' => $parameters['items-price-currency']
                    ]
                ]
            ],
            'totalAmount' => [
                'amount' => $parameters['totalAmount-amount'],
                'currency' => $parameters['totalAmount-currency']
            ],
            'merchant' => [
                'redirectCancelUrl' => $parameters['merchant-redirectCancelUrl'],
                'redirectConfirmUrl' => $parameters['merchant-redirectConfirmUrl']
            ]
        ];

        return json_encode($payload);
    }

    /**
     * This function should be implemented in order to get token from API call instead of return static string
     *
     * @return string
     */
    private function _getToken()
    {
        return $this::BEARER_TOKEN;
    }

    /**
     * @param $payload
     * @return mixed
     */
    public function send($payload)
    {
        $this->_curl->setHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->_getToken()
        ]);

        $this->_curl->post(
            $this->_configuration->getApiServiceUrl() . $this->_configuration->getApiOrderEndpoint(),
            $payload
        );

        $response = json_decode($this->_curl->getBody(), true);

        return $response;
    }
}
