<?php
namespace Scalapay\Casestudy\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * class Configuration
 *
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class Configuration extends AbstractHelper
{
    const IS_ENABLED = 'scalapay_configuration/general/is_enabled';
    const API_SERVICE_URL = 'scalapay_configuration/general/api_service_url';
    const API_ORDER_ENDPOINT = 'scalapay_configuration/general/api_order_endpoint';

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * Configuration constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled() :bool
    {
        return $this->_scopeConfig->getValue(self::IS_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getApiServiceUrl()
    {
        return $this->_scopeConfig->getValue(self::API_SERVICE_URL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getApiOrderEndpoint()
    {
        return $this->_scopeConfig->getValue(self::API_ORDER_ENDPOINT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
