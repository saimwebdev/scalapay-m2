# Scalapay Case Study

Scalapay Case Study is a Magento 2 module used to send an API request to Scalapay services in order to create an order


## Installation

- Copy into your app/code project's directory the Scalapay/Casestudy module

- Enable module
```
bin/magento module:enable Scalapay_Casestudy
bin/magento setup:upgrade
bin/magento setup:static-content:deploy
bin/magento cache:flush
bin/magento cache:clean
```


## Configuration

Navigate your admin panel to **Stores > (Settings) Configuration > Scalapay > Configuration** in which you can
enable the module and set API service URL and order API endpoint.


## Usage

Navigate to **scalapay/index/index** route and fill all fields in order to place an order with Scalapay API
