<?php
namespace Scalapay\Casestudy\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Psr\Log\LoggerInterface;
use Scalapay\Casestudy\Helper\Configuration;
use Scalapay\Casestudy\Helper\Webservice;
use Scalapay\Casestudy\Helper\Validator;

/**
 * class Save
 *
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class Save extends Action
{
    /**
     * @var Configuration
     */
    private $_configuration;

    /**
     * @var Webservice
     */
    private $_webservice;

    /**
     * @var Validator
     */
    private $_validator;

    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @param Context $context
     * @param Configuration $configuration
     * @param Webservice $webservice
     * @param Validator $validator
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        Webservice $webservice,
        Validator $validator,
        LoggerInterface $logger
    ) {
        $this->_configuration = $configuration;
        $this->_webservice = $webservice;
        $this->_validator = $validator;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            // skip if not enabled
            if (!$this->_configuration->isEnabled()) {
                throw new \Exception('Module is disabled');
            }

            // get parameters
            $parameters = $this->getRequest()->getParams();
            if (empty($parameters)) {
                throw new \Exception('No parameters received');
            }

            // validate post parameters
            $this->_validator->validate($parameters);

            // build payload
            $payload = $this->_webservice->build($parameters);

            // send payload to Scalapay and get response
            $response = $this->_webservice->send($payload);
            if (!isset($response['errorId'])) {
                return $this->_redirectToUrl($response['checkoutUrl']);
            }
        } catch (\Exception $e) {
            $this->_logger->critical(__METHOD__ . ' | Error on sending data to Scalapay service: ');
            $this->_logger->critical($e->getMessage());
        }

        return $this->_redirectToUrl('/scalapay/index/index');
    }

    /**
     * @param $url
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    private function _redirectToUrl($url)
    {
        $result = $this->resultRedirectFactory->create();
        $result->setUrl($url);

        return $result;
    }
}

