<?php
namespace Scalapay\Casestudy\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Scalapay\Casestudy\Helper\Configuration;

/**
 * class Index
 *
 * @author Simone Gentili
 * @package Scalapay Casestudy
 */
class Index extends Action
{
    /**
     * @var Configuration
     */
    private $_configuration;

    /**
     * @param Configuration $configuration
     * @param Context $context
     */
    public function __construct(
        Configuration $configuration,
        Context $context
    ) {
        $this->_configuration = $configuration;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // if not enabled redirect to homepage
        if (!$this->_configuration->isEnabled()) {
            $result = $this->resultRedirectFactory->create();
            $result->setUrl('/');

            return $result;
        }

        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}

